(function(trans) {

  trans.TransProto.lexer = function (inputCode) {
    
	//Позиция лексемы
	this.str_pos = 0;
	this.pos_in_str = 0;
	
	//Искать лексемы пока строка с кодом не пустая
	while(inputCode.length != 0)
	{	
		var f = false; //Найдено совпадение
		var max_length = 0; //Длина совпадения
		var max_type = ""; //Тип
		for(var type in this.lexTable)//Найти самое длинное совпадение
		{
			var reg = new RegExp(this.lexTable[type].regex);
			
			var ex_res = reg.exec(inputCode);
			if(ex_res)
			{
				if(ex_res[0].length > max_length)
				{
					f = true;
					max_length = ex_res[0].length;
					max_type = type;
				}
			}
		}
	
		if(!f)//Если совпадение не найдено то вернуть ошибку
		{
			return {
				"status" : "Error",
				"error" :
				{
					"error_text" : "Ошибка в строке " + (this.str_pos + 1) + '. Не найдено совпадений в таблице',
					"prog_text" : inputCode,
					"line" : this.str_pos + 1
				}
			};
		}
		else//Если совпадение найдено то сравнить его с ключевыми словами из списка
		{
			var reg = new RegExp(this.lexTable[max_type].regex);
			var match = reg.exec(inputCode)[0];
			var obj = this.lexTable[max_type];
			this.is_list = function(obj2, match)//
			{
				var list = obj2.list;
				if(list)
				{
					for(var key in list)
					{
						for(var i = 0; i < list[key].length; i++)
						{
							if(list[key][i] == match)
							{
								return {
									"type" : max_type,
									"group" : key,
									"match" : match
								};
							}
						}
					}
				}
				//Если совпадений не найдено, то считать это переменной/названием функции/константой
				for(type in this.lexTable)
				{
					if(this.lexTable[type].link)
					{
						var reg = new RegExp(this.lexTable[type].regex);
			
						var ex_res = reg.exec(inputCode);
						if(ex_res)
						{
							max_type = type;
							obj = this.lexTable[max_type];
						}
					}
				}
				return false;
			};
			this.is_link = function(obj, match, name)
			{
				if(obj.link)
				{
					var l = obj.link.length;
					for(var i = 0; i < l; i++)
					{
						if(obj.link[i] == match)
						{
							return	{
								"type" : name,
								"value" : obj.link[i]
							};
						}
					}
					obj.link.push(match);
					return {
						"type" : name,
						"value" : obj.link[l]
 					};
				}
				return false;
			};
			this.is_skiped = function(obj)//Нужно ли пропускать лексему. Например комментарии
			{
				if(obj.skip)
				{
					return true;
				}
				else
				{
					return false;
				}
			};
			var lexem = this.is_skiped(obj) || this.is_list(obj, match) || this.is_link(obj, match, max_type);
			if(!lexem) //Если тип лексемы не определен то вернуть ошибку
			{
				return {
					"status" : "Error",
					"error" :
					{
						"error_text" : "Ошибка в строке " + (this.str_pos + 1) + '. Не определен тип лексемы',
						"token" : max_type,
						"match" : match,
						"line" : this.str_pos + 1
					}
				};			
			}
			else
			{
				if(!(lexem === true))
				{
					lexem["string"] = this.str_pos + 1;
					lexem["position"] = this.pos_in_str;
					this.lexArray.push(lexem); //Добавить в таблицу новую лексему
				}
				
				for(var i = 0; i < match.length; i++)
				{
					if(match[i] === "\n")
					{
						++this.str_pos;
						this.pos_in_str = 0;
					}
					else
					{
						this.pos_in_str++;
					}
				}
				inputCode = inputCode.replace(reg, ""); //Убрать лексему из кода, чтобы повторно её не обрабатывать
			}
		}

	}
	
	//Вернуть результат
	return {
		"status" : "Ok",
		"result" :
		{
			"idents" : this.arrayOfSymbols,
			"lexems" : this.lexArray
		}
	};

  }
  
  

})(window.trans || (window.trans = {}));