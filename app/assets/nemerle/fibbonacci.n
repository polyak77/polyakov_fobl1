def next(x) { x + 1 };

def mult(x, y) { x * y }

def fibbonacci(_)
{
  | 0     => 0
  | 1     => 1
  | i => fibbonacci(i - 1) + fibbonacci(i - 2)
}

Console.WriteLine(next(9));        // 10
Console.WriteLine(mult(2, 2));     // 4
Console.WriteLine(fibbonacci(10)); // 55